package com.example.bruno.desafioandroid_bruno.Message;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Bruno on 19/06/2015.
 */
public class Message {

    private static String text;

    public static void makeText(Context context, String s){
        text = s;
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }
}
