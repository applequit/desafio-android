package com.example.bruno.desafioandroid_bruno.Basic;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bruno.desafioandroid_bruno.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DescriptionActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private Handler handler = new Handler();

    private ImageView avatar, image_real;
    private TextView text_name, description, name_art, nick, num_lovers, num_viewers;

    ProgressDialog pDialog;
    Bitmap bitmap;

    private String textD, avatar_url, image_real_url;
    String[] cut = {"<p>", "</p>","<b>","</br>","<a href=","</a>" ,"<strong>","</strong>","rel=", "<br />", ">", "<"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);

        //imageLoader.get(shotList.get(position).getImageTeaserURL(), imageLoader.getImageListener(holder.imageView, R.drawable.ic_launcher, R.drawable.ic_launcher));

        this.text_name = (TextView) findViewById(R.id.desc_name);
        this.description = (TextView) findViewById(R.id.desc_txt);
        this.name_art = (TextView) findViewById(R.id.my_title);
        this.nick = (TextView) findViewById(R.id.nick);
        this.num_lovers = (TextView) findViewById(R.id.num_lovers);
        this.num_viewers = (TextView) findViewById(R.id.num_viewers);

        this.avatar = (ImageView) findViewById(R.id.desc_url);
        this.image_real = (ImageView) findViewById(R.id.image_real_url);

        this.textD = getIntent().getStringExtra("desc");

        this.toolbar = (Toolbar) findViewById(R.id.app_bar);
        //this.toolbar.setSubtitle("Made by: Bruno");

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        for (int i = 0; i < this.cut.length; i++)
            this.textD = textD.replaceAll(this.cut[i], " ");

        textD = textD.replace("null", "No Description.");
        this.avatar_url = getIntent().getStringExtra("avatar").toString();
        this.image_real_url = getIntent().getStringExtra("image_real").toString();

        this.text_name.setText(getIntent().getStringExtra("name"));
        this.name_art.setText(getIntent().getStringExtra("title"));
        this.nick.setText("(" + getIntent().getStringExtra("nick") + ")");
        this.num_lovers.setText(getIntent().getStringExtra("lovers"));
        this.num_viewers.setText(getIntent().getStringExtra("viewers"));

        this.description.setText(textD);



        this.downloadImage(this.avatar_url, avatar);
        this.downloadImage(this.image_real_url, image_real);

    }

    public void downloadImage(final String URL, final ImageView iv){
        new Thread(){
            public void run(){
                Bitmap img = null;

                try{
                    URL url = new URL(""+URL);

                    HttpURLConnection conexao = (HttpURLConnection) url.openConnection();
                    InputStream input = conexao.getInputStream();
                    img= BitmapFactory.decodeStream(input);
                }
                catch(IOException e){}

                final Bitmap imgAux = img;
                handler.post(new Runnable(){
                    public void run(){
                        iv.setImageBitmap(imgAux);
                    }
                });
            }
        }.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_description, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                finish();
                break;

        }

        return super.onOptionsItemSelected(item);
    }


}
