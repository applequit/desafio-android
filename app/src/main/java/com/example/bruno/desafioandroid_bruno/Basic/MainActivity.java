package com.example.bruno.desafioandroid_bruno.Basic;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.LruCache;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.bruno.desafioandroid_bruno.Adapters.CustomShotAdapter;
import com.example.bruno.desafioandroid_bruno.Network.InfoDribbble;
import com.example.bruno.desafioandroid_bruno.Network.VolleySingleton;
import com.example.bruno.desafioandroid_bruno.R;
import com.example.bruno.desafioandroid_bruno.Types.Shots;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.bruno.desafioandroid_bruno.Interfaces.ShotKey.InfoShot.KEY_DESCRIPTION;
import static com.example.bruno.desafioandroid_bruno.Interfaces.ShotKey.InfoShot.KEY_ID;
import static com.example.bruno.desafioandroid_bruno.Interfaces.ShotKey.InfoShot.KEY_IMAGE_400_URL;
import static com.example.bruno.desafioandroid_bruno.Interfaces.ShotKey.InfoShot.KEY_IMAGE_TEASER_URL;
import static com.example.bruno.desafioandroid_bruno.Interfaces.ShotKey.InfoShot.KEY_LIKES_COUNT;
import static com.example.bruno.desafioandroid_bruno.Interfaces.ShotKey.InfoShot.KEY_PAGES;
import static com.example.bruno.desafioandroid_bruno.Interfaces.ShotKey.InfoShot.KEY_SHOTS;
import static com.example.bruno.desafioandroid_bruno.Interfaces.ShotKey.InfoShot.KEY_TITLE;
import static com.example.bruno.desafioandroid_bruno.Interfaces.ShotKey.InfoShot.KEY_VIEWS_COUNT;
import static com.example.bruno.desafioandroid_bruno.Interfaces.ShotKey.InfoUser.KEY_AVATAR_URL;
import static com.example.bruno.desafioandroid_bruno.Interfaces.ShotKey.InfoUser.KEY_LOCATION;
import static com.example.bruno.desafioandroid_bruno.Interfaces.ShotKey.InfoUser.KEY_NAME;
import static com.example.bruno.desafioandroid_bruno.Interfaces.ShotKey.InfoUser.KEY_PLAYER;
import static com.example.bruno.desafioandroid_bruno.Interfaces.ShotKey.InfoUser.KEY_USERNAME;


public class MainActivity extends AppCompatActivity{

    private ListView listView;


    private Toolbar toolbar;
    private Toolbar toolbarBottom;

    private VolleySingleton volleySingleton;
    private RequestQueue queue;
    public ImageLoader imageLoader;
    private ArrayList<ImageView> page = new ArrayList<ImageView>();

    public static int num_pages = 1;
    private Integer getPages;


    public ArrayList<Shots> listShots = new ArrayList<>();
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main_appbar);
        setContentView(R.layout.activity_main);

        this.volleySingleton = VolleySingleton.getInstance();
        this.queue = VolleySingleton.getInstance().getQueue();

        this.toolbar =(Toolbar)findViewById(R.id.app_bar);
        //this.toolbar.setSubtitle("Made by: Bruno");

        setSupportActionBar(toolbar);

        //getSupportActionBar().setDisplayShowHomeEnabled(true);

        this.toolbarBottom = (Toolbar) findViewById(R.id.app_bar_bottom);

        page.add((ImageView) findViewById(R.id.action_back));
        page.add((ImageView) findViewById(R.id.action_next));


        //Pages Manager
        for(int i = 0; i<page.size(); i++) {
            final int i1 = i;
            View.OnClickListener clickListener = new View.OnClickListener() {
                public void onClick(View v) {
                    if (v.equals(page.get(i1))) {
                        switch (i1){
                            case 0:
                                num_pages = (num_pages <= 1)?1:num_pages -1;
                                break;
                            case 1:
                                num_pages = (num_pages >= getPages)?5:num_pages +1;
                                break;
                        }
                        //Message.makeText(getBaseContext(),""+ num_pages);
                        sendJsonRequest(num_pages+"");
                    }
                }
            };

            page.get(i).setOnClickListener(clickListener);
        }

        //Suporte ao NavigationDrawer
        //NavigationDrawerFragment drawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        //drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout)findViewById(R.id.drawer_layout),toolbar);

        this.sendJsonRequest(this.num_pages+"");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(this, "Hello, I'm " + item.getTitle(), Toast.LENGTH_SHORT).show();
            return true;
        }*/

        return false;
    }


    private void sendJsonRequest(String pageN){
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, this.getURL()+pageN, (String)null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        //Message.makeText(getActivity(), response.toString());
                         parseJSONResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        this.queue.add(jor);
    }

    private void parseJSONResponse(JSONObject response){
        final ArrayList<Shots> listShots = new ArrayList<>();

        if(response!=null||response.length()>0) {
            try {
                //StringBuilder data = new StringBuilder();


               // Message.makeText(getBaseContext(), response.getJSONArray(KEY_PAGES).toString());
                JSONObject n_pages = response;
                getPages = n_pages.getInt(KEY_PAGES);

                JSONArray amountShot = response.getJSONArray(KEY_SHOTS);

                for (int i = 0; i < amountShot.length(); i++) {
                    //Main Information
                    JSONObject currentShot = amountShot.getJSONObject(i);
                    long id = Long.parseLong(currentShot.getString(KEY_ID));

                    String likes_count = currentShot.getString(KEY_LIKES_COUNT);
                    String views_count = currentShot.getString(KEY_VIEWS_COUNT);
                    String title = currentShot.getString(KEY_TITLE);
                    String description = currentShot.getString(KEY_DESCRIPTION);
                    String image_teaser_url = currentShot.getString(KEY_IMAGE_TEASER_URL);
                    String image_url = currentShot.getString(KEY_IMAGE_400_URL);

                    //User Information
                    JSONObject infoUsers = currentShot.getJSONObject(KEY_PLAYER);
                    String username = null;
                    String name = null;
                    String location = null;
                    String avatar_url = null;

                    username = checkUser(infoUsers, username, KEY_USERNAME);
                    name = checkUser(infoUsers, name, KEY_NAME);
                    location = checkUser(infoUsers, location, KEY_LOCATION);
                    avatar_url = checkUser(infoUsers, avatar_url, KEY_AVATAR_URL);

                    //data.append(id + " " + title + "" +/*""+ image_teaser_url+ " "+*/username + "\n");

                    Shots shots = new Shots();
                    //Primary Screen
                    shots.setId(id);
                    shots.setLikesCount(likes_count);
                    shots.setViewsCount(views_count);
                    shots.setTitle(title);
                    shots.setDescription(description);
                    shots.setImageTeaserURL(image_teaser_url);
                    //Secondary Screen
                    shots.setUsername(username);
                    shots.setName(name);
                    shots.setLocation(location);
                    shots.setAvatarURL(avatar_url);
                    shots.setImageURL(image_url);

                    listShots.add(shots);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        imageLoader = new ImageLoader(queue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> cache = new LruCache<String, Bitmap>(10);

            @Override
            public Bitmap getBitmap(String url) {
                return cache.get(url);
            }

            @Override
            public void putBitmap(String url, Bitmap bitmap) {
                cache.put(url, bitmap);
            }
        });

        this.listShots = listShots;

        //ListView Implements
        listView =(ListView) findViewById(R.id.listView);
        listView.setAdapter(new CustomShotAdapter(this, listShots, imageLoader));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                            Intent intent = new Intent(MainActivity.this, DescriptionActivity.class);
                            intent.putExtra("name", listShots.get((int)id).getName());
                            intent.putExtra("desc", listShots.get((int)id).getDescription());
                            intent.putExtra("avatar", listShots.get((int)id).getAvatarURL());
                            intent.putExtra("image_real", listShots.get((int)id).getImageURL());
                            intent.putExtra("title", listShots.get((int)id).getTitle());
                            intent.putExtra("nick", listShots.get((int)id).getUsername());
                            intent.putExtra("lovers", listShots.get((int)id).getLikesCount());
                            intent.putExtra("viewers", listShots.get((int)id).getViewsCount());
                            startActivity(intent);
            }
        });

        //RecyclerView
        /*
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container,
                ShotFragment.newInstance(null, null)).
                addToBackStack("").commit();
       */
    }


    private String checkUser(JSONObject js, String s, String set_key) throws JSONException {
        if(js.has(set_key))
            s = js.getString(set_key);
        else
            s = "empty";

        return s;
    }

    private String getURL(){
        return InfoDribbble.SHOT_POPULAR;
    }
}
