package com.example.bruno.desafioandroid_bruno.Basic;

import android.app.Application;
import android.content.Context;

/**
 * Created by Bruno on 19/06/2015.
 */
public class MyApp extends Application {
    private static MyApp instance;

    @Override
    public void onCreate(){
        super.onCreate();

        instance = this;
    }

    public static MyApp getInstance(){
        return instance;
    }

    public static Context getAppContext(){
        return instance.getApplicationContext();
    }
}
