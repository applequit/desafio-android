package com.example.bruno.desafioandroid_bruno.Types;

/**
 * Created by Bruno on 19/06/2015.
 */
public class Shots {

    private long id;

    private String title;
    private String description;
    private String imageTeaserURL;

    private String imageURL;
    private String name;
    private String username;
    private String location;
    private String avatarURL;

    private String likesCount, viewsCount;


    public Shots() {
    }

    public Shots(long id, String title, String description, String imageTeaserURL, String imageURL, String name, String username, String location, String avatarURL, String likesCount, String viewsCount) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.imageTeaserURL = imageTeaserURL;
        this.imageURL = imageURL;
        this.name = name;
        this.username = username;
        this.location = location;
        this.avatarURL = avatarURL;
        this.likesCount = likesCount;
        this.viewsCount = viewsCount;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getViewsCount() {
        return viewsCount;
    }

    public void setViewsCount(String viewsCount) {
        this.viewsCount = viewsCount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageTeaserURL() {
        return imageTeaserURL;
    }

    public void setImageTeaserURL(String imageTeaserURL) {
        this.imageTeaserURL = imageTeaserURL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(String likesCount) {
        this.likesCount = likesCount;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    public void setAvatarURL(String avatarURL) {
        this.avatarURL = avatarURL;
    }

    public String toString(){
        return "ID: "+id+
                "Title: "+title+
                "Description: "+description+
                "ImageTeaserURL"+imageTeaserURL+
                "Name: "+name+
                "Username: "+username+
                "Location: "+location+
                "AvatarURL: "+avatarURL+
                "LikesCount: "+likesCount+
                "ViewsCount: "+viewsCount+
                "ImageURL"+imageURL;
    }

}
