package com.example.bruno.desafioandroid_bruno.Interfaces;

/**
 * Created by Bruno on 19/06/2015.
 */
public interface ShotKey {
    public interface InfoShot {
        public static final String KEY_PAGES = "pages";
        public static final String KEY_SHOTS = "shots";
        public static final String KEY_ID = "id";
        public static final String KEY_TITLE = "title";
        public static final String KEY_DESCRIPTION = "description";
        public static final String KEY_LIKES_COUNT = "likes_count";
        public static final String KEY_VIEWS_COUNT = "views_count";
        public static final String KEY_IMAGE_TEASER_URL ="image_teaser_url";
        public static final String KEY_IMAGE_400_URL ="image_400_url";
    }

    public interface InfoUser {
        public static final String KEY_PLAYER = "player";
        public static final String KEY_NAME = "name";
        public static final String KEY_USERNAME = "username";
        public static final String KEY_AVATAR_URL = "avatar_url";
        public static final String KEY_LOCATION = "location";
    }
}
