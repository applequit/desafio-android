package com.example.bruno.desafioandroid_bruno.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.bruno.desafioandroid_bruno.R;
import com.example.bruno.desafioandroid_bruno.Types.Shots;

import java.util.List;

/**
 * Created by Bruno on 21/06/2015.
 */
public class RecyclerShotAdapter extends RecyclerView.Adapter<RecyclerShotAdapter.myViewHolder> {

    List<Shots> myShots;
    LayoutInflater layoutInflater = null;
    private ImageLoader imageLoader;

    public RecyclerShotAdapter(Context c, List<Shots> shots,ImageLoader imageLoader){
        this.myShots = shots;
        this.imageLoader = imageLoader;
        this.layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.e("log", "qts");

        View v = layoutInflater.inflate(R.layout.shot_list, parent, false);
        myViewHolder myViewHolder = new myViewHolder(v);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(myViewHolder holder, int position){
        holder.networkImageView.setImageUrl(myShots.get(position).getImageTeaserURL(),imageLoader);
        holder.networkImageView.setDefaultImageResId(R.drawable.ic_launcher);
        holder.networkImageView.setErrorImageResId(R.drawable.ic_launcher);

        holder.title.setText(myShots.get(position).getTitle());
        holder.subTitle.setText(myShots.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return myShots.size();
    }

    public void addList(Shots shot, int position){
        myShots.add(shot);
        notifyItemInserted(position);
    }

    public class myViewHolder extends RecyclerView.ViewHolder{

        public NetworkImageView networkImageView;
        public TextView title, subTitle;

        public myViewHolder(View itemView) {
            super(itemView);

            this.networkImageView = (NetworkImageView) itemView.findViewById(R.id.key_teaser_url);
            this.title = (TextView) itemView.findViewById(R.id.key_title);
            this.subTitle = (TextView) itemView.findViewById(R.id.key_sub_title);
        }
    }
}
