package com.example.bruno.desafioandroid_bruno.Network;

import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.bruno.desafioandroid_bruno.Basic.MyApp;

/**
 * Created by Bruno on 19/06/2015.
 */
public class VolleySingleton {
    private static VolleySingleton instance = null;

    public static VolleySingleton getInstance(){
        if(instance == null)
            instance = new VolleySingleton();
        return  instance;
    }

    private RequestQueue queue;
    private ImageLoader imageLoader;

    private VolleySingleton() {
        this.queue = Volley.newRequestQueue(MyApp.getAppContext());
        this.imageLoader = new ImageLoader(this.queue, new ImageLoader.ImageCache() {

            private LruCache <String,Bitmap> cache = new LruCache<>((int) (Runtime.getRuntime().maxMemory()/1024/8));
            @Override
            public Bitmap getBitmap(String url) {

                return cache.get(url);
            }

            @Override
            public void putBitmap(String url, Bitmap bitmap) {
                cache.put(url, bitmap);
            }
        });

    }

    public RequestQueue getQueue(){
        return this.queue;
    }

    public ImageLoader loader(){
        return this.imageLoader;
    }
}
