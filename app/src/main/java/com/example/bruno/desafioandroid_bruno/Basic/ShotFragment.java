package com.example.bruno.desafioandroid_bruno.Basic;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.toolbox.ImageLoader;
import com.example.bruno.desafioandroid_bruno.Adapters.RecyclerShotAdapter;
import com.example.bruno.desafioandroid_bruno.R;
import com.example.bruno.desafioandroid_bruno.Types.Shots;

import java.util.List;

/**
 * Created by Bruno on 21/06/2015.
 */
public class ShotFragment extends Fragment{

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private RecyclerView myRecyclerView;
    List<Shots> myShots;
    ImageLoader imgLoader;

    public ShotFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public static ShotFragment newInstance(String param1, String param2) {
        ShotFragment fragment = new ShotFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //RecyclerView
        View view = inflater.inflate(R.layout.fragment_recycler_shot, container, false);

        this.myRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);


        Bundle args = getArguments();
        ShotFragment myClass = (ShotFragment) args
                .getSerializable("");

        //this.myRecyclerView.setHasFixedSize(true);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        this.myRecyclerView.setLayoutManager(layoutManager);

        myShots = ((MainActivity) getActivity()).listShots;
        imgLoader = ((MainActivity)getActivity()).imageLoader;

        RecyclerShotAdapter shotAdapter = new RecyclerShotAdapter(getActivity(), myShots, imgLoader);
        myRecyclerView.setAdapter(shotAdapter);

        super.onViewCreated(view, savedInstanceState);
    }
}
