package com.example.bruno.desafioandroid_bruno.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.bruno.desafioandroid_bruno.R;
import com.example.bruno.desafioandroid_bruno.Types.Shots;

import java.util.List;

/**
 * Created by Bruno on 20/06/2015.
 */

public class CustomShotAdapter extends BaseAdapter {

    private List<Shots> shotList;
    private ImageLoader imageLoader;
    private LayoutInflater inflater=null;

    public CustomShotAdapter(Context context,List<Shots> shots, ImageLoader imageLoader) {
        this.shotList = shots;
        this.imageLoader = imageLoader;

        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return shotList.size();
    }

    @Override
    public Object getItem(int position) {
        return shotList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null)
        {
            convertView = inflater.inflate(R.layout.shot_list, null);
            holder = new ViewHolder();
            convertView.setTag(holder);

            holder.networkImageView = (NetworkImageView) convertView.findViewById(R.id.key_teaser_url);
            holder.imageView = (ImageView) convertView.findViewById(R.id.key_teaser_url);
            holder.textView = (TextView) convertView.findViewById(R.id.key_title);
            holder.subTitle = (TextView) convertView.findViewById(R.id.key_sub_title);
        }
        else
        {
            holder = (ViewHolder)convertView.getTag();
        }


        //Network
        holder.networkImageView.setImageUrl(shotList.get(position).getImageTeaserURL(),imageLoader);
        holder.networkImageView.setDefaultImageResId(R.drawable.ic_launcher);
        holder.networkImageView.setErrorImageResId(R.drawable.ic_launcher);


        //ImageView
        //imageLoader.get(shotList.get(position).getImageTeaserURL(), imageLoader.getImageListener(holder.imageView, R.drawable.ic_launcher, R.drawable.ic_launcher));

        holder.textView.setText(shotList.get(position).getTitle());
        holder.subTitle.setText("("+shotList.get(position).getName()+")");

        //Message.makeText(convertView.getContext(), "Entrei");

        return convertView;
    }

    public static class ViewHolder{
        public NetworkImageView networkImageView;
        public ImageView imageView;
        public TextView textView, subTitle;
    }

}
